<?php

namespace lst\CoreBundle\Service\GeoProviders;

use lst\CoreBundle\Abstractions\Interfaces\Geo\LocationInterface;
use RuntimeException;

class Sypexgeo implements LocationInterface
{
    /** @var string  */
    private $baseUrl = 'http://api.sypexgeo.net/';

    /** @var string */
    public $ip;
    /** @var string */
    public $countryIso;
    /** @var string */
    public $cityName;
    /** @var string */
    public $cityId;
    /** @var string */
    public $regionId;

    public function determineLocation(): void
    {
        if (!$this->ip) {
            throw new RuntimeException("ip address is not defined");
        }

        $this->baseUrl .=  "json/" . $this->ip;
        $locationInfo = json_decode(file_get_contents($this->baseUrl), true);

        $this->setInfo($locationInfo);
    }

    /**
     * @param $locationInfo
     */
    private function setInfo($locationInfo): void
    {
        $this->countryIso = $locationInfo['country']['iso'] ?? "";
        $this->cityName = $locationInfo['city']['name_ru'] ?? "";
        $this->cityId = $locationInfo['city']['okato'] ?? "";

        $regionId = "";
        if ($this->cityId) {
            $regionId = substr($this->cityId, 0, 2);
        }
        $this->regionId = $regionId;
    }

    /**
     * @return string
     */
    public function getCountryIso(): string
    {
        return $this->countryIso;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @return string
     */
    public function getCityName(): string
    {
        return $this->cityName;
    }

    /**
     * @return string
     */
    public function getCityId(): string
    {
        return $this->cityId;
    }

    /**
     * @return string
     */
    public function getRegionId(): string
    {
        return $this->regionId;
    }

    /**
     * @param $ip
     */
    public function setIp(string $ip): void
    {
        $this->ip = $ip;
    }
}