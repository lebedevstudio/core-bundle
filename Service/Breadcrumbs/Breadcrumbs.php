<?php

declare(strict_types=1);

namespace lst\CoreBundle\Service\Breadcrumbs;

use lst\CoreBundle\Interfaces\EntityTypeInterface;

class Breadcrumbs
{
    /** @var string */
    public const KEY = 'breadcrumbs';

    /** @var array */
    private $crumbs = [];

    /**
     * @param Crumb $crumb
     */
    public function addCrumb(Crumb $crumb) : void
    {
        $this->crumbs[] = $crumb;
    }

    /**
     * @return array
     */
    public function getCrumbs() : array
    {
        return array_reverse($this->crumbs);
    }
}
