<?php

declare(strict_types=1);

namespace lst\CoreBundle\Service\Breadcrumbs;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Crumb
{
    /** @var int */
    private $id;
    /** @var string */
    private $title;
    /** @var string */
    private $alias;
    /**
     * @var int
     * @SerializedName("entity_type_id")
     */
    private $entityTypeId;

    public function __construct(int $id, string $title, string $alias, int $entityTypeId)
    {
        $this->id = $id;
        $this->title = $title;
        $this->alias = $alias;
        $this->entityTypeId = $entityTypeId;
    }

    /**
     * @return int
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getAlias() : string
    {
        return $this->alias;
    }

    /**
     * @return int
     */
    public function getEntityTypeId() : int
    {
        return $this->entityTypeId;
    }
}
