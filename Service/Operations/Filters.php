<?php

declare(strict_types=1);

namespace lst\CoreBundle\Service\Operations;

class Filters
{
    /** @var array */
    private $filters = [];
    /** @var array  */
    private $order = [];
    /** @var int|null */
    private $limit = null;
    /** @var int|null */
    private $offset = null;
    /** @var string|null */
    private $group = null;

    public function __construct(array $filters)
    {
        $this->filters = $filters;

        $this->prepareOrder();
        $this->prepareLimit();
        $this->prepareOffset();
        $this->prepareFilters();
        $this->prepareGroup();
    }

    /**
     * @return array
     */
    public function getFilters() : array
    {
        return $this->filters;
    }

    /**
     * @return array
     */
    public function getOrder() : array
    {
        return $this->order;
    }

    /**
     * @return int|null
     */
    public function getLimit() : ?int
    {
        return $this->limit;
    }

    /**
     * @return int|null
     */
    public function getOffset() : ?int
    {
        return $this->offset;
    }

    /**
     * @return string|null
     */
    public function getGroup() : ?string
    {
        return $this->group;
    }

    /**
     * @return string
     */
    public function getLocale() : string
    {
        return $this->filters['locale'] ?? '';
    }

    /**
     * @param string $param
     * @return bool
     */
    private function checkValueIsArray(string $param) : bool
    {
        return strpos($param, ',') > 0 ? true : false;
    }

    /**
     * @param mixed $param
     * @return bool
     */
    private function checkValueIsNull($param) : bool
    {
        return ($param == 'null') ? true : false;
    }

    private function prepareLimit() : void
    {
        if (isset($this->filters['limit'])) {
            $this->limit = (int)$this->filters['limit'];
            unset($this->filters['limit']);
        }
    }

    private function prepareOffset() : void
    {
        if (isset($this->filters['offset'])) {
            $this->offset = (int)$this->filters['offset'];
            unset($this->filters['offset']);
        }
    }

    private function prepareOrder() : void
    {
        if (isset($this->filters['order'])) {
            $this->order = $this->filters['order'];
            unset($this->filters['order']);
        } else {
            $this->order = ['createdAt' => 'DESC'];
        }

    }

    private function prepareGroup()
    {
        if (isset($this->filters['group'])) {
            $this->group = $this->filters['group'];
            unset($this->filters['group']);
        }
    }

    /**
     * @param string $value
     * @return array
     */
    private function prepareArray(string $value) : array
    {
        $values = explode(',', $value);
        foreach ($values as &$value) {
            $value =  preg_replace('/[^a-zA-z0-9]/', '', $value);
        }

        return $values;
    }

    private function prepareFilters() : void
    {
        foreach ($this->filters as $filter => &$value) {
            if ($value == null) {
                return;
            }
            if ($this->checkValueIsArray($value)) {
                $value = $this->prepareArray($value);
            }
            if ($this->checkValueIsNull($value)) {
                $value = null;
            }
        }
    }
}
