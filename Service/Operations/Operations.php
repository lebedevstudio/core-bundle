<?php

declare(strict_types=1);

namespace lst\CoreBundle\Service\Operations;

use Doctrine\ORM\EntityManagerInterface;
use lst\CoreBundle\Exception\EntityIdNotSpecified;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\CoreBundle\Service\Links\Links;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Operations
{
    /** @var ValidatorInterface */
    protected $validator;
    /** @var SerializerInterface */
    protected $serializer;
    /** @var EntityManagerInterface */
    protected $manager;
    /** @var Links */
    protected $links;

    public function __construct(ValidatorInterface $validator, SerializerInterface $serializer,
        EntityManagerInterface $manager, Links $links)
    {
        $this->validator = $validator;
        $this->serializer = $serializer;
        $this->manager = $manager;
        $this->links = $links;
    }

    /**
     * @param string $class
     * @param array  $filters
     * @return array
     */
    public function list(string $class, Filters $filters)
    {
// TODO move to query builder

//        $qb = $this->manager->createQueryBuilder();

//        if (!empty($columns)) {
//            $array = explode(',', $columns);
//            foreach ($array as &$column) {
//                $column = 'd.' . preg_replace('/[^A-Za-z]/', '', $column);
//            }
//            $qb->select($array);
//        } else {
//            $qb->select('d');
//        }
//
//        $qb->from($class, 'd');
//        if ($limit > 0) {
//           $qb->setMaxResults($limit);
//        }
//
//        $qb->orderBy('d.createdAt', 'DESC');
//
//        return $qb->getQuery()->getResult();


//        return $x = $qb->select(['d.id', 'd.title', 'd.subtitle'])
//            ->getQuery()->getResult();

        $data = $this->manager->getRepository($class)->findBy(
            $filters->getFilters(),
            $filters->getOrder(),
            $filters->getLimit(),
            $filters->getOffset()
        );

        # TODO Refactor
        if ($group = $filters->getGroup()) {
            $getter = 'get' . ucfirst($group);
            $grouped = [];
            foreach ($data as $item => $value) {
                if (property_exists($value, $group)) {
                    $grouped[$value->$getter()->getId()][$group] = $value->$getter();
                    $grouped[$value->$getter()->getId()]['group'][] = $value;
                } else {
                    throw new \RuntimeException(sprintf('Group field %s not found in result data', $group));
                }
            }
            $data = $grouped;

        }

        return $data;
    }

    public function getLinks(EntityTypeInterface $entity)
    {
        return $this->links->find($entity->getId(), $entity::getentitytypeid());
    }

    /**
     * @param string $class
     * @param string $data
     * @param string $method
     * @return mixed
     */
    public function persist(string $class, string $data, string $method)
    {
        if ($method == 'PUT') {
            $requestData = json_decode($data, true);
            if (!isset($requestData['id']) || (isset($requestData['id']) && !is_int($requestData['id']))) {
                throw new EntityIdNotSpecified();
            }
        }
        $entity = $this->deserialize($data, $class, 'json');
        $errors = $this->validate($entity);
        if ($errors->count() == 0) {
            $response= $this->manager->merge($entity);
            $this->manager->flush();
        } else {
            $response = $errors;
        }

        return $response;
    }

    /**
     * @param EntityTypeInterface $object
     * @return bool
     */
    public function delete(EntityTypeInterface $object) : bool
    {
        $this->manager->remove($object);
        $this->manager->flush();

        return true;
    }

    public function validate($entity)
    {
        return $this->validator->validate($entity);
    }
    
    public function deserialize(string $json, string $class, string $type = 'json')
    {
        return $this->serializer->deserialize($json, $class, $type);
    }
}
