<?php

declare(strict_types=1);

namespace lst\CoreBundle\Service\Links;

use lst\CoreBundle\Entity\Link;
use lst\CoreBundle\Repository\LinkRepository;

class Links
{
    /** @var LinkRepository */
    private $repository;
    /** @var Map */
    private $map;

    public function __construct(LinkRepository $repository, Map $map)
    {
        $this->repository = $repository;
        $this->map = $map;
    }

    public function find(int $id, int $type)
    {
        return $this->repository->findBy([
            'targetEntity' => $id,
            'targetEntityType' => $type
        ]);

//        foreach ($groups as $type => $values) {
//            $class = $this->map->getByTypeId($type);
//            $this->links[$class] = $this->em->getRepository($class)->findBy(['id' => $values]);
//        }
//        return $this->groupLinks($links);
    }

    /**
     * @param array $links
     * @return array
     */
    private function groupLinks(array $links) : array
    {
        $groups = [];
        /** @var Link $link */
        foreach ($links as $link) {
            $groups[$link->getLinkEntityType()][] = $link->getLinkEntity();
        }

        return $groups;
    }
}
