<?php

declare(strict_types=1);

namespace lst\CoreBundle\Service\Links;

class Map
{
    private $map = [];

    public function __construct()
    {
        $this->map = [
            1 => "Статья",
            2 => "Категория статей",
            4 => "Товар",
            3 => "Категория товаров",
            9 => "Файл",
            8 => "Вакансия",
            10 => "Галерея",
            11 => "Клиент",
            15 => "Сотрудник",
        ];
    }

    public function get() : array
    {
        return $this->map;
    }

    public function getByTypeId(int $id)
    {
        return (isset($this->map[$id])) ? $this->map[$id] : null;
    }
    
    public function add(int $id, string $entity) : void
    {
        if (!isset($this->map[$id])) {
            $this->map[$id] = $entity;
        }
    }
}
