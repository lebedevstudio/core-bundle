<?php

declare(strict_types=1);

namespace lst\CoreBundle\Security;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function onLogoutSuccess(Request $request)
    {
        return new JsonResponse([
            'result' => 'OK'
        ], 200);
    }
}
