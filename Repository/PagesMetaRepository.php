<?php

namespace lst\CoreBundle\Repository;

use lst\CoreBundle\Entity\PageMeta;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PageMeta|null find($id, $lockMode = null, $lockVersion = null)
 * @method PageMeta|null findOneBy(array $criteria, array $orderBy = null)
 * @method PageMeta[]    findAll()
 * @method PageMeta[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PagesMetaRepository extends ServiceEntityRepository
{
    /** @var \Doctrine\ORM\EntityManager */
    private $em;

    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();
        parent::__construct($registry, PageMeta::class);
    }

    public function persist(PageMeta $meta) : PageMeta
    {
        $entry = $this->em->merge($meta);
        $this->em->flush();

        return $entry;
    }
}
