<?php

namespace lst\CoreBundle\Repository;

use lst\CoreBundle\Entity\LinkGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LinkGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method LinkGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method LinkGroup[]    findAll()
 * @method LinkGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LinkGroupRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LinkGroup::class);
    }
}
