<?php

namespace lst\CoreBundle\Repository;

use lst\CoreBundle\Entity\Registry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Registry|null find($id, $lockMode = null, $lockVersion = null)
 * @method Registry|null findOneBy(array $criteria, array $orderBy = null)
 * @method Registry[]    findAll()
 * @method Registry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegistryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Registry::class);
    }
}
