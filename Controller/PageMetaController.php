<?php

declare(strict_types=1);

namespace lst\CoreBundle\Controller;

use lst\CoreBundle\Entity\PageMeta;
use lst\CoreBundle\Repository\PagesMetaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;

class PageMetaController extends AbstractController
{
    /** @var RequestStack */
    private $request;
    /** @var ValidatorInterface */
    private $validator;
    /** @var PagesMetaRepository */
    private $pagesMetaRepository;
    /** @var SerializerInterface */
    private $serializer;
    /** @var NormalizerInterface */
    private $normalizer;
    /** @var array */
    private $serializationGroups = ['basic'];
    /** @var int */
    private $responseStatus = 200;

    public function __construct(
        RequestStack $request,
        ValidatorInterface $validator,
        PagesMetaRepository $pagesMetaRepository,
        SerializerInterface $serializer,
        NormalizerInterface $normalizer
    )
    {
        $this->request = $request->getCurrentRequest();
        $this->validator = $validator;
        $this->pagesMetaRepository = $pagesMetaRepository;
        $this->serializer = $serializer;
        $this->normalizer = $normalizer;

        $this->serializationGroups = array_merge(
            $this->request->query->get('with', []),
            $this->serializationGroups
        );
    }

    /**
     * @Route("/core/pages-meta/{id}", name="core.page-meta.get", methods={"GET"}, requirements={"id"="\d+"})
     * @param PageMeta $pageMeta
     *
     * @return JsonResponse
     */
    public function getPageMeta(PageMeta $pageMeta) : JsonResponse
    {
        return new JsonResponse([
            PageMeta::SINGLE_KEY => $this->normalizer->normalize($pageMeta, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }

    /**
     * @Route("/core/pages-meta", name="core.page-meta.create", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function createPageMeta() : JsonResponse
    {
        return $this->persistEntity();
//        $pageMeta = $this->serializer->deserialize(
//            $this->request->getContent(),
//            PageMeta::class,
//            'json'
//        );
//        $errors = $this->validator->validate($pageMeta);
//        if ($errors->count() == 0) {
//            $response= $this->pagesMetaRepository->persist($pageMeta);
//        } else {
//            $response = $errors;
//            $this->responseStatus = 502;
//        }
//
//        return new JsonResponse([
//            PageMeta::SINGLE_KEY => $this->normalizer->normalize($response, 'array', [
//                'groups' => $this->serializationGroups
//            ])
//        ], $this->responseStatus);
    }

    /**
     * @Route("/core/pages-meta/{id}", name="core.page-meta.update", methods={"PUT"}, requirements={"id"="\d+"})
     *
     * @return JsonResponse
     */
    public function updatePageMeta() : JsonResponse
    {
        return $this->persistEntity();
//        $pageMeta = $this->serializer->deserialize(
//            $this->request->getContent(),
//            PageMeta::class,
//            'json'
//        );
//        $errors = $this->validator->validate($pageMeta);
//        if ($errors->count() == 0) {
//            $response= $this->pagesMetaRepository->persist($pageMeta);
//        } else {
//            $response = $errors;
//            $this->responseStatus = 502;
//        }
//
//        return new JsonResponse([
//            PageMeta::SINGLE_KEY => $this->normalizer->normalize($response, 'array', [
//                'groups' => $this->serializationGroups
//            ])
//        ], $this->responseStatus);
    }

    private function persistEntity() : JsonResponse
    {
        $pageMeta = $this->serializer->deserialize(
            $this->request->getContent(),
            PageMeta::class,
            'json'
        );
        $errors = $this->validator->validate($pageMeta);
        if ($errors->count() == 0) {
            $response= $this->pagesMetaRepository->persist($pageMeta);
        } else {
            $response = $errors;
            $this->responseStatus = 502;
        }

        return new JsonResponse([
            PageMeta::SINGLE_KEY => $this->normalizer->normalize($response, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }
}
