<?php

declare(strict_types=1);

namespace lst\CoreBundle\Controller;

use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Entity\City;
use lst\CoreBundle\Service\Operations\Operations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CityController extends AbstractController
{
    /** @var Operations */
    protected $operations;
    /** @var string */
    private $entity = City::class;
    /** @var string */
    private $entitySingleKey = City::SINGLE_KEY;
    /** @var string */
    private $entityMultipleKey = City::MULTIPLE_KEY;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/geo/cities",
     *     name="core.geo.city.list",
     *     methods={"GET"}
     * )
     *
     * @return JsonResponse
     */
    public function listEntity() : JsonResponse
    {
        return $this->list($this->entity, $this->entityMultipleKey);
    }

    /**
     * @Route(
     *     "/geo/cities/{city}",
     *     name="core.geo.city.get",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @param City $city
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function getEntity(City $city) : JsonResponse
    {
        return new JsonResponse([
            $this->entitySingleKey => $this->normalizer->normalize($city, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }

    /**
     * @Route(
     *     "/geo/cities",
     *     name="core.geo.city.create",
     *     methods={"POST"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function createEntity(): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/geo/cities/{id}",
     *     name="core.geo.city.update",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param City $entity
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function updateEntity(City $entity): JsonResponse
    {
        return $this->persist(
            $this->entity,
            $this->entitySingleKey,
            $this->request->getContent(),
            $this->request->getMethod()
        );
    }

    /**
     * @Route(
     *     "/geo/cities/{id}",
     *     name="core.geo.city.delete",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param City $entity
     *
     * @return JsonResponse
     */
    public function deleteEntity(City $entity): JsonResponse
    {
        return $this->delete($entity);
    }
}
