<?php

declare(strict_types=1);

namespace lst\CoreBundle\Controller;

use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Entity\Region;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class RegionController extends AbstractController
{
    /** @var Operations */
    protected $operations;
    /** @var string */
    private $entity = Region::class;
    /** @var string */
    private $entitySingleKey = Region::SINGLE_KEY;
    /** @var string */
    private $entityMultipleKey = Region::MULTIPLE_KEY;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route("/geo/regions", name="core.geo.region.list", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function listEntity() : JsonResponse
    {
        return $this->list($this->entity, $this->entityMultipleKey);
    }

    /**
     * @Route(
     *     "/geo/regions/{region}",
     *     name="core.geo.region.get", methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @return JsonResponse
     */
    public function getEntity(Region $region) : JsonResponse
    {
        return new JsonResponse([
            $this->entitySingleKey => $this->normalizer->normalize($region, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }
}
