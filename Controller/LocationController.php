<?php
declare(strict_types=1);

namespace lst\CoreBundle\Controller;

use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Abstractions\Interfaces\Geo\LocationInterface;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class LocationController extends AbstractController
{
    /** @var Operations */
    protected $operations;

    /** @var LocationInterface */
    private $location;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request, LocationInterface $location)
    {
        $this->operations = $operations;
        $this->location = $location;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route(
     *     "/geo/location",
     *     name="core.geo.location",
     *     methods={"GET"}
     * )
     *
     * @return JsonResponse
     */
    public function getLocationByIp(): JsonResponse
    {
        $request = Request::createFromGlobals();
        $ip = $request->server->get('HTTP_X_REAL_IP') ?? $this->request->getClientIp();

        $this->location->setIp($ip);
        $this->location->determineLocation();

        $locationInfo = $this->getLocationInfo();

        return new JsonResponse($locationInfo, $this->responseStatus);
    }

    /**
     * @Route(
     *     "/geo/location/{ip}",
     *     name="core.geo.location.external_ip",
     *     methods={"GET"}
     * )
     * @param  string  $ip
     * @return JsonResponse
     */
    public function getLocationWithExternalIp(string $ip): JsonResponse
    {
        $this->location->setIp($ip);
        $this->location->determineLocation();

        $locationInfo = $this->getLocationInfo();

        return new JsonResponse($locationInfo, $this->responseStatus);
    }

    /**
     * @return array
     */
    private function getLocationInfo(): array
    {
        return
            [
                'ip' => $this->location->getIp(),
                'countryIso' => $this->location->getCountryIso(),
                'cityName' => $this->location->getCityName(),
                'cityId' => $this->location->getCityId(),
                'regionId' => $this->location->getRegionId()
            ];
    }
}