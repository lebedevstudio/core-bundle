<?php

declare(strict_types=1);

namespace lst\CoreBundle\Controller;

use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Entity\Link;
use lst\CoreBundle\Service\Links\Map;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class LinksController extends AbstractController
{
    /** @var Operations */
    protected $operations;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route("/links", name="core.link.create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     */
    public function createLink() : JsonResponse
    {
        return $this->persist(Link::class, Link::SINGLE_KEY, $this->request->getContent() ,$this->request->getMethod());
    }

    /**
     * @Route("/links/{id}", name="core.link.update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Link $link
     * @return JsonResponse
     */
    public function updateLink(Link $link) : JsonResponse
    {
        return $this->persist(Link::class, Link::SINGLE_KEY, $this->request->getContent() ,$this->request->getMethod());
    }

    /**
     * @Route("/links/{id}", name="core.link.delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Link $link
     * @return JsonResponse
     */
    public function deleteLink(Link $link) : JsonResponse
    {
        return $this->delete($link);
    }

    /**
     * @Route("/links/map", name="core.link.get.map", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function getMap() : JsonResponse
    {
        return new JsonResponse([
            'map' => (new Map())->get()
        ], $this->responseStatus);
    }
}
