<?php

declare(strict_types=1);

namespace lst\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class PingController extends AbstractController
{
    /**
     * @Route("/", name="ping-pong")
     *
     * @return JsonResponse
     */
    public function index() : JsonResponse
    {
        return new JsonResponse(['pong']);
    }
}
