<?php

declare(strict_types=1);

namespace lst\CoreBundle\Controller;

use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Entity\District;
use lst\CoreBundle\Service\Operations\Operations;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class DistrictController extends AbstractController
{
    /** @var Operations */
    protected $operations;
    /** @var string */
    private $entity = District::class;
    /** @var string */
    private $entitySingleKey = District::SINGLE_KEY;
    /** @var string */
    private $entityMultipleKey = District::MULTIPLE_KEY;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route("/geo/districts", name="core.geo.district.list", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function listEntity() : JsonResponse
    {
        return $this->list($this->entity, $this->entityMultipleKey);
    }

    /**
     * @Route(
     *     "/geo/districts/{district}",
     *     name="core.geo.district.get", methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @return JsonResponse
     */
    public function getEntity(District $district) : JsonResponse
    {
        return new JsonResponse([
            $this->entitySingleKey => $this->normalizer->normalize($district, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }
}
