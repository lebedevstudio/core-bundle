<?php

declare(strict_types=1);

namespace lst\CoreBundle\Controller;

use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Entity\LinkGroup;
use lst\CoreBundle\Service\Operations\Operations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class LinksGroupsController extends AbstractController
{
    /** @var Operations */
    protected $operations;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
    * @Route("/links/groups", name="core.link.group.list", methods={"GET"})
    * @IsGranted("ROLE_ADMIN")
    *
    * @param Link $link
    * @return JsonResponse
    */
    public function listLinkGroup() : JsonResponse
    {
        return $this->list(LinkGroup::class, LinkGroup::MULTIPLE_KEY);
    }

    /**
     * @Route("/links/groups", name="core.link.group.create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     */
    public function createLink() : JsonResponse
    {
        return $this->persist(LinkGroup::class, LinkGroup::SINGLE_KEY, $this->request->getContent() ,$this->request->getMethod());
    }

    /**
     * @Route("/links/groups/{id}", name="core.link.group.update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Link $link
     * @return JsonResponse
     */
    public function updateLink(LinkGroup $linkGroup) : JsonResponse
    {
        return $this->persist(LinkGroup::class, LinkGroup::SINGLE_KEY, $this->request->getContent() ,$this->request->getMethod());
    }

    /**
     * @Route("/links/groups/{id}", name="core.link.group.delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Link $link
     * @return JsonResponse
     */
    public function deleteLink(LinkGroup $linkGroup) : JsonResponse
    {
        return $this->delete($linkGroup);
    }
}
