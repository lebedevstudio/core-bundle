<?php

declare(strict_types=1);

namespace lst\CoreBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use http\Client\Curl\User;
use lst\CoreBundle\Entity\Admin;
use lst\CoreBundle\Entity\Token;
use lst\CoreBundle\Repository\AdminRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController
{
    /** @var RequestStack */
    private $request;
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(RequestStack $request, EntityManagerInterface $em)
    {
        $this->request = $request;
        $this->em = $em;
    }

    /**
     * @Route("/auth/login", name="core.auth.login", methods={"POST"})
     */
    public function login()
    {
        /** @var \lst\CoreBundle\Entity\Admin $user */
        $user = $this->getUser();
        $token = new Token($user);

        $this->em->persist($token);
        $this->em->flush();

        return $this->json([
            'Authorization' => $token->getKey(),
            'expire' => $token->getExpire()->format('c'),
        ]);
    }

    /**
     * @Route("/auth/logout", name="core.auth.logout", methods={"POST"})
     */
    public function logout()
    {
        // Symfony magic goes here
    }

    /**
     * @Route("/auth", name="core.auth.get.user", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getLoggedUser()
    {
        $user = $this->getUser();

        return new JsonResponse([
            'username' => $user->getUsername(),
        ], 200);
    }
}
