<?php

declare(strict_types=1);

namespace lst\CoreBundle\Validator\Constraints;

use lst\CoreBundle\Interfaces\EntityTypeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Mapping\PropertyMetadata;

class UniqueFieldValidator extends ConstraintValidator
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var EntityTypeInterface */
    private $entity = null;
    /** @var PropertyMetadata */
    private $metaData = null;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function validate($value, Constraint $constraint)
    {
        if ($value == '') {
            return;
        }
        $this->entity = $this->context->getObject();
        $this->metaData = $this->context->getMetadata();
        $query = $this->em->createQueryBuilder();
        $unique = true;

        $query->select('t')
            ->from($this->metaData->class, 't')
            ->where('t.' . $this->metaData->property . ' = :value')
            ->setParameter(':value', $value);

        if ($constraint->with) {
            $query->andWhere('t.' . $constraint->with . ' = :with');
            $propertyGetter = 'get' . ucfirst($constraint->with);
            $propertyValue = $this->entity->{$propertyGetter}();
            $query->setParameter(':with', $propertyValue);
        }

        $entry = $query->getQuery()->getOneOrNullResult();
        if ($entry != null) {
            if ($entry->getId() !== $this->entity->getId()) {
                $unique = false;
            }
        }

        if (!$unique) {
            if ($constraint->with) {
                $this->context
                    ->buildViolation($constraint->compoundFieldsMessage)
                    ->setCode(UniqueField::NOT_UNIQUE_ERROR)
                    ->setParameter(':property', $this->metaData->property . '(' . $value  . ')')
                    ->setParameter(':with', $constraint->with . '(' . $propertyValue . ')')
                    ->addViolation();
            } else if (!$constraint->with) {
                $this->context
                    ->buildViolation($constraint->singleFieldMessage)
                    ->setCode(UniqueField::NOT_UNIQUE_ERROR)
                    ->setParameter(':property', $this->metaData->property . '(' . $value  . ')')
                    ->addViolation();
            }
        }
    }
}
