<?php

declare(strict_types=1);

namespace lst\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueField extends Constraint
{
    const NOT_UNIQUE_ERROR = '00000000-d103-4f74-8988-acbcafc7fdc3';

    /** @var string */
    public $with = '';

    protected static $errorNames = [
        self::NOT_UNIQUE_ERROR => 'NOT_UNIQUE_ERROR',
    ];

    public $singleFieldMessage = 'Entity with `:property` already exists.';

    public $compoundFieldsMessage = 'Entity with `:property` and `:with` already exists.';
}
