<?php

namespace lst\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTimeImmutable;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="core_admins_tokens")
 * @ORM\Entity(repositoryClass="lst\CoreBundle\Repository\TokenRepository")
 */
class Token
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="lst\CoreBundle\Entity\Admin")
     */
    private $admin;

     /**
      * @ORM\Column(type="string", unique=true, nullable=false)
      */
     private $key;

    /**
     * @ORM\Column(type="date_immutable", nullable=false)
     */
     private $expire;

     public function __construct($admin)
     {
         $this->admin = $admin;
         $this->key = md5(time());
         $this->expire = new \DateTimeImmutable('tomorrow');
         $this->createdAt = new \DateTimeImmutable();
     }

     public function getId() : ?int
     {
         return $this->id;
     }

     public function getAdmin() : Admin
     {
         return $this->admin;
     }

     public function getKey() : string
     {
         return $this->key;
     }

     public function getExpire() : DateTimeImmutable
     {
         return $this->expire;
     }
}
