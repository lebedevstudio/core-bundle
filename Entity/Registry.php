<?php

declare(strict_types=1);

namespace lst\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="core_registry")
 * @ORM\Entity(repositoryClass="lst\CoreBundle\Repository\RegistryRepository")
 */
class Registry
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $title = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $keywords = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $description = '';
}
