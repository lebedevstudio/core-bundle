<?php

declare(strict_types=1);

namespace lst\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use Symfony\Component\Serializer\Annotation\Groups;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\AbstractEntity;
use Symfony\Component\Validator\Constraints as Assert;
use lst\CoreBundle\Validator\Constraints as Asserts;

/**
 * Region Entity
 *
 * @ORM\Table(name="core_geo_regions")
 * @ORM\Entity(repositoryClass="lst\CoreBundle\Repository\RegionRepository")
 */
class Region extends AbstractEntity
{
    use Timestampable, Activity, ExternalId;

    /** @var int */
    protected const ENTITY_TYPE_ID = 19;
    /** @var string */
    public const SINGLE_KEY = 'region';
    /** @var string */
    public const MULTIPLE_KEY = 'regions';

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Groups({"basic"})
     */
    private $title;

    /**
     * @Assert\Type(
     *     type="District"
     * )
     *
     * @ORM\ManyToOne(targetEntity="District", inversedBy="regions")
     * @Groups({"district"})
     */
    private $district;

    /**
     * @Assert\Type(
     *     type="City"
     * )
     *
     * @ORM\OneToMany(targetEntity="City", mappedBy="region")
     * @var Collection
     * @Groups({"cities"})
     */
    private $cities;

    /**
     * @ORM\Column(type="string")
     * @Asserts\UniqueField()
     * @Assert\NotBlank()
     * @Groups({"basic"})
     */
    private $okatoId;

    public function __construct()
    {
        $this->cities = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return District
     */
    public function getDistrict(): District
    {
        return $this->district;
    }

    /**
     * @return Collection|City[]
     */
    public function getCities(): Collection
    {
        return $this->cities;
    }

    /**
     * @return string
     */
    public function getOkatoId(): string
    {
        return $this->okatoId;
    }
}
