<?php

declare(strict_types=1);

namespace lst\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use Symfony\Component\Serializer\Annotation\Groups;
use lst\CoreBundle\Interfaces\EntityTypeInterface;

/**
 * @ORM\Table(name="core_pages_meta")
 * @ORM\Entity(repositoryClass="lst\CoreBundle\Repository\PagesMetaRepository")
 */
class PageMeta extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable;

    /** @var int */
    protected const ENTITY_TYPE_ID = 6;
    /** @var string */
    public const SINGLE_KEY = 'pageMeta';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"basic"})
     */
    private $title = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"basic"})
     */
    private $keywords = '';

    /**
     * @ORM\Column(
     *     type="text",
     *     nullable=false,
     *     options={"default":""}
     * )
     * @Groups({"basic"})
     */
    private $description = '';

    /**
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false,
     *     options={"default":""}
     * )
     * @Groups({"basic"})
     */
    private $h1 = '';

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(string $title = ''): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getKeywords() : string
    {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     */
    public function setKeywords(string $keywords = ''): void
    {
        $this->keywords = $keywords;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description = ''): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getH1(): string
    {
        return $this->h1;
    }

    /**
     * @param  string  $h1
     */
    public function setH1(string $h1): void
    {
        $this->h1 = $h1;
    }
}
