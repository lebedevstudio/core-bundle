<?php

declare(strict_types=1);

namespace lst\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use lst\CoreBundle\Abstractions\Traits\Activity;
use Symfony\Component\Serializer\Annotation\Groups;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\AbstractEntity;
use Symfony\Component\Validator\Constraints as Assert;
use lst\CoreBundle\Validator\Constraints as Asserts;

/**
 * District Entity
 *
 * @ORM\Table(name="core_geo_districts")
 * @ORM\Entity(repositoryClass="lst\CoreBundle\Repository\DistrictRepository")
 */
class District extends AbstractEntity
{
    use Timestampable, Activity;

    /** @var int */
    protected const ENTITY_TYPE_ID = 18;
    /** @var string */
    public const SINGLE_KEY = 'district';
    /** @var string */
    public const MULTIPLE_KEY = 'districts';

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @Asserts\UniqueField()
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Groups({"basic"})
     */
    private $title;

    /**
     * @Assert\Type(
     *     type="Region"
     * )
     *
     * @ORM\OneToMany(targetEntity="Region", mappedBy="district")
     * @var Collection
     * @Groups({"regions"})
     */
    private $regions;

    /**
     * @Assert\Type(
     *     type="City"
     * )
     *
     * @ORM\OneToMany(targetEntity="City", mappedBy="region")
     * @var Collection
     * @Groups({"cities"})
     */
    private $cities;

    public function __construct()
    {
        $this->regions = new ArrayCollection();
        $this->cities = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Collection|Region[]
     */
    public function getRegions(): Collection
    {
        return $this->regions;
    }

    /**
     * @return Collection|City[]
     */
    public function getCities(): Collection
    {
        return $this->cities;
    }
}
