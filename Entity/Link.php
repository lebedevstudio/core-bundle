<?php

declare(strict_types=1);

namespace lst\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="core_links")
 * @ORM\Entity(repositoryClass="lst\CoreBundle\Repository\LinkRepository")
 */
class Link extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable;

    /** @var int */
    protected const ENTITY_TYPE_ID = 11;
    /** @var string */
    public const SINGLE_KEY = 'link';
    /** @var string */
    public const MULTIPLE_KEY = 'links';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Groups({"basic"})
     */
    protected $targetEntity;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Groups({"basic"})
     */
    protected $targetEntityType;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Groups({"basic"})
     */
    protected $linkEntity;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Groups({"basic"})
     */
    protected $linkEntityType;

    /**
     * @ORM\ManyToOne(targetEntity="LinkGroup")
     * @Assert\Valid()
     * @Groups({"basic"})
     */
    protected $group = null;

    /**
     * @return int
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getTargetEntity() : int
    {
        return $this->targetEntity;
    }

    /**
     * @param integer $targetEntity
     */
    public function setTargetEntity(int $targetEntity): void
    {
        $this->targetEntity = $targetEntity;
    }

    /**
     * @return integer
     */
    public function getTargetEntityType() : int
    {
        return $this->targetEntityType;
    }

    /**
     * @param integer $targetEntityType
     */
    public function setTargetEntityType(int $targetEntityType): void
    {
        $this->targetEntityType = $targetEntityType;
    }

    /**
     * @return integer
     */
    public function getLinkEntity() : int
    {
        return $this->linkEntity;
    }

    /**
     * @param integer $linkEntity
     */
    public function setLinkEntity(int $linkEntity) : void
    {
        $this->linkEntity = $linkEntity;
    }

    /**
     * @return integer
     */
    public function getLinkEntityType() : int
    {
        return $this->linkEntityType;
    }

    /**
     * @param integer $linkEntityType
     */
    public function setLinkEntityType(int $linkEntityType) : void
    {
        $this->linkEntityType = $linkEntityType;
    }

    /**
     * @return LinkGroup
     */
    public function getGroup() : ?LinkGroup
    {
        return $this->group;
    }

    /**
     * @param LinkGroup $group
     */
    public function setGroup(?LinkGroup $group) : void
    {
        $this->group = $group;
    }
}
