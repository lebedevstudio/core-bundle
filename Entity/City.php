<?php

declare(strict_types=1);

namespace lst\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use lst\CoreBundle\Abstractions\Traits\Activity;
use lst\CoreBundle\Abstractions\Traits\Translatable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Abstractions\AbstractEntity;
use Symfony\Component\Validator\Constraints as Assert;
use lst\CoreBundle\Validator\Constraints as Asserts;


/**
 * City Entity
 *
 * @ORM\Table(name="core_geo_cities")
 * @ORM\Entity(repositoryClass="lst\CoreBundle\Repository\CityRepository")
 */
class City extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, Activity, Translatable;

    /** @var int */
    protected const ENTITY_TYPE_ID = 20;
    /** @var string */
    public const SINGLE_KEY = 'city';
    /** @var string */
    public const MULTIPLE_KEY = 'cities';

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Groups({"basic"})
     */
    private $title;

    /**
     * @Assert\Type(
     *     type="lst\CoreBundle\Entity\Region"
     * )
     *
     * @ORM\ManyToOne(targetEntity="lst\CoreBundle\Entity\Region", inversedBy="cities")
     * @Assert\NotBlank()
     * @Groups({"region"})
     */
    private $region;

    /**
     * @Assert\NotBlank()
     * @Asserts\UniqueField()
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     * @Groups({"basic"})
     */
    private $okatoId;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return Region
     */
    public function getRegion(): Region
    {
        return $this->region;
    }

    /**
     * @param Region $region
     */
    public function setRegion(Region $region): void
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getOkatoId(): string
    {
        return $this->okatoId;
    }

    /**
     * @param string $okatoId
     */
    public function setOkatoId(string $okatoId): void
    {
        $this->okatoId = $okatoId;
    }
}
