<?php

declare(strict_types=1);

namespace lst\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\CoreBundle\Validator\Constraints as Asserts;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="core_links_groups")
 * @ORM\Entity(repositoryClass="lst\CoreBundle\Repository\LinkGroupRepository")
 */
class LinkGroup extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable;

    /** @var int */
    protected const ENTITY_TYPE_ID = 12;
    /** @var string */
    public const SINGLE_KEY = 'linkGroup';
    /** @var string */
    public const MULTIPLE_KEY = 'linksGroups';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Groups({"basic"})
     */
    protected $title;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Asserts\UniqueField()
     * @Groups({"basic"})
     */
    protected $alias;

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getAlias() : string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias(string $alias) : void
    {
        $this->alias = $alias;
    }
}
