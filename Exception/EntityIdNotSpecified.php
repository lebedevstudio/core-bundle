<?php

declare(strict_types=1);

namespace lst\CoreBundle\Exception;

class EntityIdNotSpecified extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('Entity ID not specified', 501, null);
    }
}
