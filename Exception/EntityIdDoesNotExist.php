<?php

declare(strict_types=1);

namespace lst\CoreBundle\Exception;

class EntityIdDoesNotExist extends \RuntimeException
{
    public function __construct($data, $class)
    {
        parent::__construct("Entity id = {$data} in {$class} does not exist", 501, null);
    }
}