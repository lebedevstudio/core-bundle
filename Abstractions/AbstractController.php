<?php

declare(strict_types=1);

namespace lst\CoreBundle\Abstractions;

use lst\CoreBundle\Interfaces\EntityTypeInterface;
use lst\CoreBundle\Service\Operations\Filters;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class AbstractController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    /** @var RequestStack */
    protected $request;
    /** @var NormalizerInterface */
    protected $normalizer;
    /** @var array */
    protected $serializationGroups = ['basic'];
    /** @var int */
    protected $responseStatus = 200;
    /** @var Filters */
    protected $filters = [];

    public function __construct(NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->normalizer = $normalizer;
        $this->request = $request->getCurrentRequest();
        $filters = $this->request->query->get('filters', []);
        if ($locale = $this->request->headers->get('Locale')) {
            $filters['locale'] = $locale;
        }

        $this->filters = new Filters($filters);
        $this->serializationGroups = array_merge(
            $this->request->query->get('with', []),
            $this->serializationGroups
        );
    }

    /**
     * @param string $entity
     * @param string $key
     *
     * @return JsonResponse
     */
    protected function list(string $entity, string $key) : JsonResponse
    {
        return new JsonResponse([
            $key => $this->normalizer->normalize(
                $this->operations->list(
                    $entity,
                    $this->filters
                ),
                'array', [
                    'groups' => $this->serializationGroups,
                ]
            )
        ], $this->responseStatus);
    }

    /**
     * @param EntityTypeInterface $entity
     * @return object|void
     */
    public function getLinks(EntityTypeInterface $entity)
    {
        return $this->operations->getLinks($entity);
    }

    /**
     * @param string $class
     * @param string $key
     * @param string $data
     * @param string $method
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    protected function persist(string $class, string $key, string $data, string $method) : JsonResponse
    {
        $this->addLocale($data);
        $operation = $this->operations->persist($class, $data, $method);
        $this->checkValidationErrorResponse($operation);

        return new JsonResponse([
            $key => $this->normalizer->normalize(
                $operation,
                'array', [
                    'groups' => $this->serializationGroups,
                ]
            )
        ], $this->responseStatus);
    }

    protected function persistAndReturnEntity(string $class, string $key, string $data, string $method)
    {
        $this->addLocale($data);
        $operation = $this->operations->persist($class, $data, $method);
        $this->checkValidationErrorResponse($operation);

        return $operation;
    }

    /**
     * @param EntityTypeInterface $entity
     *
     * @return JsonResponse
     */
    protected function delete(EntityTypeInterface $entity) : JsonResponse
    {
        $this->operations->delete($entity);

        return new JsonResponse([
            'result' => 'OK'
        ], $this->responseStatus);
    }

    private function checkValidationErrorResponse($operation) : void
    {
        if ($operation instanceof \Symfony\Component\Validator\ConstraintViolationList) {
            $this->responseStatus = 502;
        }
    }

    private function addLocale(&$data)
    {
        if ($locale = $this->request->headers->get('Locale')) {
            $json = json_decode($data, true);
            $json['locale'] = $locale;
            $data = json_encode($json);
        }

        return $data;
    }
}
