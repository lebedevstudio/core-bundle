<?php

declare(strict_types=1);

namespace lst\CoreBundle\Abstractions\Traits;

trait Activity
{
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":true})
     * @Groups({"basic", "all"})
     */
    protected $active = true;

    /**
     * @return bool
     */
    public function getActive() : bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active) : void
    {
        $this->active = $active;
    }
}
