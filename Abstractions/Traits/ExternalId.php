<?php

declare(strict_types=1);

namespace lst\CoreBundle\Abstractions\Traits;

trait ExternalId
{
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"basic", "all"})
     */
    protected $externalId;

    /**
     * @return int|null
     */
    public function getExternalId() : ?int
    {
        return $this->externalId;
    }

    /**
     * @param int|null $externalId
     */
    public function setExternalId(?int $externalId) : void
    {
        $this->externalId = $externalId;
    }
}
