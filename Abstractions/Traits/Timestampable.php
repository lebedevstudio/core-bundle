<?php

declare(strict_types=1);

namespace lst\CoreBundle\Abstractions\Traits;

use Gedmo\Mapping\Annotation as Gedmo;

trait Timestampable
{
    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default": "CURRENT_TIMESTAMP"})
     * @Gedmo\Timestampable(on="create")
     * @Groups({"basic", "all"})
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Groups({"basic", "all"})
     */
    protected $updatedAt = null;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    /**
     * @return string
     */
    public function getCreatedAt() : string
    {
        return $this->createdAt->format('c');
    }

    /**
     * @return string
     */
    public function getUpdatedAt() : string
    {
        return ($this->updatedAt == null) ? '' : $this->updatedAt->format('c');
    }
}
