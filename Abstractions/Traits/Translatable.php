<?php

declare(strict_types=1);

namespace lst\CoreBundle\Abstractions\Traits;

trait Translatable
{
    /**
     * @ORM\Column(type="string", length=2, nullable=false, options={"default": "ru"})
     * @Groups({"basic", "all"})
     */
    protected $locale = 'ru';

    /**
     * @return string
     */
    public function getLocale() : string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale(string $locale) : void
    {
        $this->locale = $locale;
    }
}
