<?php

declare(strict_types=1);

namespace lst\CoreBundle\Abstractions;

class AbstractEntity
{
    protected const ENTITY_TYPE_ID = 0;

    public static function getEntityTypeId() : int
    {
        return static::ENTITY_TYPE_ID;
    }
}
