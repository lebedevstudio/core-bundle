<?php

namespace lst\CoreBundle\Abstractions\Interfaces\Geo;

interface LocationInterface
{
    /**
     * @return string
     */
    public function getIp() : string;

    /**
     * @return string
     */
    public function getCountryIso(): string;

    /**
     * @return string
     */
    public function getCityName() : string;

    /**
     * @return string
     */
    public function getCityId() : string;

    /**
     * @return string
     */
    public function getRegionId() : string;
}