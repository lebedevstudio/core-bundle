<?php

declare(strict_types=1);

namespace lst\CoreBundle\Interfaces;

interface EntityTypeInterface
{
    public static function getEntityTypeId() : int;
}
