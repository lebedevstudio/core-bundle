<?php

declare(strict_types=1);

namespace lst\CoreBundle\EventListener\Reply;

class Validation implements ReplyCodeInterface
{
    /** @var int */
    public $status = 502;
    /** @var string */
    public $message = 'Validation error';

    public $errors = [];

    public function __construct($errors)
    {
        $violations = array_pop($errors);
        if (isset($violations['violations'])) {
            foreach ($violations['violations'] as $error) {
                $this->errors[] = [
                    'code'    => $error['type'],
                    'message' => ucfirst($error['propertyPath']) . ', ' . strtolower($error['title']),
                ];
            }
        }
    }

    public function getStatusCode() : int
    {
        return $this->status;
    }
}
