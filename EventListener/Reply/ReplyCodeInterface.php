<?php

declare(strict_types=1);

namespace lst\CoreBundle\EventListener\Reply;

interface ReplyCodeInterface
{
    public function getStatusCode() : int;
}
