<?php

declare(strict_types=1);

namespace lst\CoreBundle\EventListener\Reply;

class BadAuthentication implements ReplyCodeInterface
{
    /** @var int */
    public $status = 403;
    /** @var string */
    public $message = 'Not allowed';

    public $errors = [];

    public function __construct($errors)
    {
        $this->errors[] = $errors;
    }

    public function getStatusCode() : int
    {
        return $this->status;
    }
}
