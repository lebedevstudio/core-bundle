<?php

declare(strict_types=1);

namespace lst\CoreBundle\EventListener\Reply;

class OK implements ReplyCodeInterface
{
    /** @var int */
    public $status = 200;
    /** @var string */
    public $message = 'OK';

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getStatusCode() : int
    {
        return $this->status;
    }
}
