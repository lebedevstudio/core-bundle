<?php

declare(strict_types=1);

namespace lst\CoreBundle\EventListener\Reply;

class NotFound implements ReplyCodeInterface
{
    /** @var int */
    public $status = 404;
    /** @var string */
    public $message = 'Entity Not found';

    public $errors = [];

    public function __construct($error)
    {
        $this->errors[] = $error;
    }

    public function getStatusCode() : int
    {
        return $this->status;
    }
}
