<?php

declare(strict_types=1);

namespace lst\CoreBundle\EventListener\Reply;

class ReplyFactory
{
    private $map = [
        200 => 'lst\CoreBundle\EventListener\Reply\OK',
        404 => 'lst\CoreBundle\EventListener\Reply\NotFound',
        401 => 'lst\CoreBundle\EventListener\Reply\BadAuthentication',
        403 => 'lst\CoreBundle\EventListener\Reply\BadAuthorization',
        501 => 'lst\CoreBundle\EventListener\Reply\RuntimeError',
        502 => 'lst\CoreBundle\EventListener\Reply\Validation',
    ];

    public function createReply($response)
    {
        $rCode = $response->getStatusCode();

        if (isset($this->map[$rCode])) {
            return new $this->map[$rCode](json_decode($response->getContent(), true));
        }
    }
}
