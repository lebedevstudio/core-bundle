<?php

declare(strict_types=1);

namespace lst\CoreBundle\EventListener\Reply;

class BadAuthorization implements ReplyCodeInterface
{
    /** @var int */
    public $status = 401;
    /** @var string */
    public $message = 'Access denied';

    public $errors = [];

    public function __construct($errors)
    {
        $this->errors[] = $errors;
    }

    public function getStatusCode() : int
    {
        return $this->status;
    }
}
