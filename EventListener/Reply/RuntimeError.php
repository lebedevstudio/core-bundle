<?php

declare(strict_types=1);

namespace lst\CoreBundle\EventListener\Reply;

class RuntimeError implements ReplyCodeInterface
{
    /** @var int */
    public $status = 501;
    /** @var string */
    public $message = 'Runtime error';

    public $errors = [];

    public function __construct($data)
    {
        $this->errors[] = $data;
    }

    public function getStatusCode() : int
    {
        return $this->status;
    }
}
