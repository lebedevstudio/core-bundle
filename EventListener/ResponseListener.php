<?php

declare(strict_types=1);

namespace lst\CoreBundle\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use lst\CoreBundle\EventListener\Reply\ReplyFactory;

class ResponseListener
{
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $reply = (new ReplyFactory())
            ->createReply($event->getResponse());

        $event->setResponse(new JsonResponse($reply, 200));
    }
}
