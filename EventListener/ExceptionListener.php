<?php

declare(strict_types=1);

namespace lst\CoreBundle\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $code = $this->getExceptionCode($exception);
        $message = $this->getExceptionMessage($exception);

        $errors = [
            'code'    => $code,
            'message' => $message,
            'line'    => $exception->getLine(),
            'file'    => $exception->getFile(),
        ];

        $event->setResponse(new JsonResponse($errors, $code));
        $event->stopPropagation();
    }

    private function getExceptionCode($exception) : int {

        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            $exceptionCode = $exception->getStatusCode();
        } else {
            $exceptionCode =  $exception->getCode() == 0 ? 501 : $exception->getCode();
        }

        return $exceptionCode;
    }

    private function getExceptionMessage($exception): string
    {
        if ($exception instanceof \Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException) {
            $sqlStateCode = $exception->getSQLState();

            if ($sqlStateCode == '23503') {
                return 'Fail to delete related entity';
            } else {
                return 'Foreign key violation error';
            }

        } else {
            return $exception->getMessage();
        }
    }
}